package com.airlift.app

import android.app.Application
import com.airlift.app.data.SessionManager
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Airlift : Application() {

    override fun onCreate() {
        super.onCreate()
        SessionManager.init(this)
    }

}
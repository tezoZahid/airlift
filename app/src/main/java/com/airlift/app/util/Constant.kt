package com.airlift.app.util

import com.airlift.app.data.responses.ProductResponseItem

object Constant {

    internal const val k_SERVER_IP = "https://fakestoreapi.com/"

    internal const val k_AUTH_TOKEN = "authToken"
    internal const val k_Success = "Success"
    internal const val k_Error = "Error"
    internal const val k_Confirmation = "Confirmation"

    var mList: List<ProductResponseItem> = mutableListOf()

}
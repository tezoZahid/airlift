package com.airlift.app.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.airlift.app.R
import com.airlift.app.data.network.Resource
import com.airlift.app.data.responses.ProductResponseItem
import com.airlift.app.databinding.ItemGeneralAlertDialogBinding
import com.google.android.material.snackbar.Snackbar

fun <A : Activity> Activity.openActivity(
    activity: Class<A>,
    newAct: Boolean = true,
    extras: Bundle.() -> Unit = {}
) {

    val intent = Intent(this, activity)
    intent.putExtras(Bundle().apply(extras))
    if (newAct) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
    }
    startActivity(intent)

}

fun View.visible(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun View.enable(enabled: Boolean) {
    isEnabled = enabled
    alpha = if (enabled) 1f else 0.5f
}

fun View.snackBar(message: String, action: (() -> Unit)? = null) {
    val snackBar = Snackbar.make(this, message, 5000)
    action?.let {
        snackBar.setAction("Retry") {
            it()
        }
    }
    snackBar.show()
}

inline fun Context.toast(message: () -> String) {
    Toast.makeText(this, message(), Toast.LENGTH_LONG).show()
}

inline fun Fragment.toast(message: () -> String) {
    Toast.makeText(this.context, message(), Toast.LENGTH_LONG).show()
}

fun FragmentActivity.displayPopUp(
    title: String,
    subTitle: String?,
    onClick: GeneralListener? = null,
    btnText: String = resources.getString(R.string.cancel)
) {
    val binding = ItemGeneralAlertDialogBinding.inflate(layoutInflater)
    val builder = AlertDialog.Builder(this)
    builder.setView(binding.root)
    val show = builder.show()

    binding.tvTitlePopUp.text = title
    binding.tvSubTitlePopUp.text = subTitle

    when (title) {
        resources.getString(R.string.error) -> {
            binding.header.setBackgroundColor(ContextCompat.getColor(this, R.color.error))
            binding.btnOk.setBackgroundColor(ContextCompat.getColor(this, R.color.error))
            binding.ivTopPopUp.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_failure
                )
            )
        }
        resources.getString(R.string.success) -> {
            binding.header.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.green
                )
            )
            binding.btnOk.setBackgroundColor(ContextCompat.getColor(this, R.color.green))
            binding.ivTopPopUp.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_success
                )
            )
        }
        resources.getString(R.string.confirmation) -> {
            binding.tvTitlePopUp.text = resources.getString(R.string.caution)
            binding.header.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.black
                )
            )
            binding.btnOk.setBackgroundColor(ContextCompat.getColor(this, R.color.black))
            binding.ivTopPopUp.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_warning
                )
            )
            binding.btnOk.text = btnText
        }
        else -> {
            binding.header.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.green
                )
            )
            binding.btnOk.setBackgroundColor(ContextCompat.getColor(this, R.color.green))
        }
    }

    binding.btnOk.setOnClickListener {
        if (onClick == null) {
            show.cancel()
        } else {
            onClick.buttonClick(true)
            show.cancel()
        }
    }

}

fun FragmentActivity.handleApiError(
    failure: Resource.Failure
) {
    val errorMessage = when {
        failure.isNetworkError -> {
            "Please check your internet connection."
        }
        failure.errorCode == 401 || failure.errorCode == 400 -> {
            "Not Authorized"
        }
        failure.throwable?.isNotEmpty() == true -> {
            failure.throwable
        }
        else -> {
            "Error"
        }
    }

    displayPopUp(
        Constant.k_Error,
        errorMessage
    )
}

fun getProductDetails(id: Int?): ProductResponseItem{
    var productResponseItem = ProductResponseItem()
    for (i in Constant.mList.indices){
        if (id == Constant.mList[i].id){
            productResponseItem = Constant.mList[i]
            break
        }
    }
    return productResponseItem
}
package com.airlift.app.data.repository

import com.airlift.app.data.network.Api
import com.airlift.app.data.network.SafeApiCall
import javax.inject.Inject

class ProductRepository @Inject constructor(
    private val api: Api
) : SafeApiCall {

    suspend fun getProducts() = safeApiCall {
        api.getProducts()
    }

}
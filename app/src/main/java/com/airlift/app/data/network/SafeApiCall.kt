package com.airlift.app.data.network

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.net.SocketTimeoutException

interface SafeApiCall {

    suspend fun <T> safeApiCall(
        apiCall: suspend () -> T
    ): Resource<T> {
        return withContext(Dispatchers.IO) {
            try {
                Resource.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                Log.e("SafeApiCall", "safeApiCall: ${throwable.message}")
                when (throwable) {
                    is HttpException -> {
                        Resource.Failure(false, throwable.code(), throwable.response()?.errorBody())
                    }
                    is SocketTimeoutException -> {
                        Resource.Failure(false, throwable.hashCode(), null, throwable.message)
                    }
                    is IllegalStateException -> {
                        Resource.Failure(false, throwable.hashCode(), null, throwable.message)
                    }
                    else -> {
                        Resource.Failure(true, null, null, throwable.message)
                    }
                }
            }
        }
    }

}
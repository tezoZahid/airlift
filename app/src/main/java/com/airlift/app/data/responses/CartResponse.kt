package com.airlift.app.data.responses

import com.google.gson.annotations.SerializedName

data class CartItem(
	@field:SerializedName("quantity")
	var quantity: Int? = null,

	@field:SerializedName("productId")
	var productId: Int? = null
)

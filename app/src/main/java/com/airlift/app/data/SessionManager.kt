package com.airlift.app.data

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.airlift.app.data.responses.CartItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


object SessionManager {

    private lateinit var prefs: SharedPreferences
    private const val PREFS_NAME = "params"

    fun init(context: Context) {
        prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    fun putString(key: String, value: String) {
        if (key == "") {
            throw IllegalArgumentException("Key-Value pair cannot be blank or null")
        }
        val prefsEditor: SharedPreferences.Editor = prefs.edit()
        with(prefsEditor) {
            putString(key, value)
            commit()
        }
    }

    fun putBoolean(key: String, value: Boolean) {
        if (key == "") {
            throw IllegalArgumentException("Key-Value pair cannot be blank or null")
        }
        val prefsEditor: SharedPreferences.Editor = prefs.edit()
        with(prefsEditor) {
            putBoolean(key, value)
            commit()
        }
    }

    fun putInt(key: String, value: Int) {
        if (key == "") {
            throw IllegalArgumentException("Key-Value pair cannot be blank or null")
        }
        val prefsEditor: SharedPreferences.Editor = prefs.edit()
        with(prefsEditor) {
            putInt(key, value)
            commit()
        }
    }

    fun remove(key: String) {
        if (key == "") {
            throw IllegalArgumentException("Key-Value pair cannot be blank or null")
        }
        val prefsEditor: SharedPreferences.Editor = prefs.edit()
        with(prefsEditor) {
            remove(key)
            commit()
        }
    }

    fun getBoolean(key: String?): Boolean {
        return prefs.getBoolean(key, false)
    }

    fun getInt(key: String?): Int {
        return prefs.getInt(key, 0)
    }

    fun getString(key: String?): String? {
        return prefs.getString(key, "")
    }

    fun insertCartItem(item: CartItem): Boolean {
        val gson = Gson()
        val prefsEditor: SharedPreferences.Editor = prefs.edit()

        if (getString("cartItem")?.isEmpty() == true) {
            Log.e("SessionManager", "insertCartItem: FirstItem")
            val json = gson.toJson(mutableListOf(item))
            return with(prefsEditor) {
                putString("cartItem", json)
                commit()
            }
        }

        val type = object : TypeToken<MutableList<CartItem?>?>() {}.type
        val mList: MutableList<CartItem> = gson.fromJson(getString("cartItem"), type)

        var itemExist = false
        for (i in mList.indices) {
            if (item.productId == mList[i].productId) {
                mList[i].quantity = mList[i].quantity?.plus(1)
                itemExist = true
                break
            }
        }
        if (!itemExist) {
            mList.add(item)
        }

        Log.e("SessionManager", "insertCartItem: $mList")
        val json = gson.toJson(mList)
        return with(prefsEditor) {
            putString("cartItem", json)
            commit()
        }
    }

    fun minusCartItem(item: CartItem): Boolean {
        val gson = Gson()
        val prefsEditor: SharedPreferences.Editor = prefs.edit()

        val type = object : TypeToken<MutableList<CartItem?>?>() {}.type
        val mList: MutableList<CartItem> = gson.fromJson(getString("cartItem"), type)

        for (i in mList.indices) {
            if (item.productId == mList[i].productId) {
                mList[i].quantity = mList[i].quantity?.minus(1)
                break
            }
        }

        Log.e("SessionManager", "insertCartItem: $mList")
        val json = gson.toJson(mList)
        return with(prefsEditor) {
            putString("cartItem", json)
            commit()
        }
    }

    fun removeItemFromCart(item: CartItem): Boolean{
        val gson = Gson()
        val prefsEditor: SharedPreferences.Editor = prefs.edit()

        val type = object : TypeToken<MutableList<CartItem?>?>() {}.type
        val mList: MutableList<CartItem> = gson.fromJson(getString("cartItem"), type)

        mList.remove(item)

        Log.e("SessionManager", "insertCartItem: $mList")
        val json = gson.toJson(mList)
        return with(prefsEditor) {
            putString("cartItem", json)
            commit()
        }
    }

    fun getCart(): MutableList<CartItem> {
        val gson = Gson()
        return if (prefs.getString("cartItem", "nothing") == "nothing") {
            mutableListOf()
        } else {
            val type = object : TypeToken<MutableList<CartItem?>?>() {}.type
            return gson.fromJson(getString("cartItem"), type)
        }
    }

}
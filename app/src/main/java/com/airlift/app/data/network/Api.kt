package com.airlift.app.data.network

import com.airlift.app.data.responses.ProductResponseItem
import retrofit2.http.GET

interface Api {

    @GET("products")
    suspend fun getProducts(): List<ProductResponseItem>

}
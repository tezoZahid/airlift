package com.airlift.app.ui.fragment.cart

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.airlift.app.R
import com.airlift.app.data.SessionManager
import com.airlift.app.data.responses.CartItem
import com.airlift.app.databinding.ItemCartBinding
import com.airlift.app.util.getProductDetails
import com.bumptech.glide.Glide

class CartAdapter(
    private val activity: FragmentActivity,
    private val mList: MutableList<CartItem>
) : RecyclerView.Adapter<CartAdapter.CartVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CartVH(
        ItemCartBinding
            .inflate(LayoutInflater.from(activity), parent, false)
    )

    override fun onBindViewHolder(holder: CartVH, position: Int) = holder.run {
        holder.bind(mList[position])
    }

    override fun getItemCount() = mList.size

    inner class CartVH(private val binding: ItemCartBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: CartItem?) {
            getProductDetails(item?.productId).let {
                it.image.let { image ->
                    Glide.with(activity)
                        .load(image)
                        .placeholder(R.drawable.ic_image_error_placeholder)
                        .fitCenter()
                        .into(binding.ivProduct)
                }
                it.title?.let { binding.tvProductName.text = it }
                it.price?.let { binding.tvItemTotal.text = "Rs: ${item?.quantity?.times(it)}" }
            }
            item?.quantity?.let { binding.tvQtyMyOrder.text = it.toString() }
            binding.btnPlusMyOrders.setOnClickListener {
                SessionManager.insertCartItem(item!!)
                item.quantity = item.quantity?.plus(1)
                notifyItemChanged(adapterPosition)
            }
            binding.btnMinusMyOrders.setOnClickListener {
                if (item?.quantity!! > 1) {
                    if (SessionManager.minusCartItem(item)) {
                        item.quantity = item.quantity?.minus(1)
                        notifyItemChanged(adapterPosition)
                    }
                }
            }
            binding.ivDelete.setOnClickListener {
                if (SessionManager.removeItemFromCart(item!!)) {
                    mList.remove(item)
                    notifyItemRemoved(adapterPosition)
                }
            }
        }

    }

}
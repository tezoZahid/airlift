package com.airlift.app.ui.fragment.product

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.airlift.app.R
import com.airlift.app.data.network.Resource
import com.airlift.app.data.responses.ProductResponseItem
import com.airlift.app.databinding.ProductFragmentBinding
import com.airlift.app.util.Constant
import com.airlift.app.util.handleApiError
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductFragment : Fragment(R.layout.product_fragment) {

    private val viewModel: ProductViewModel by viewModels()
    private lateinit var binding: ProductFragmentBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = ProductFragmentBinding.bind(view)

        viewModel.productResponse.observe(viewLifecycleOwner) {

            binding.shimmer.stopShimmer()
            binding.shimmer.visibility = View.GONE

            when (it) {
                is Resource.Success -> {
                    Constant.mList = it.value
                    populateRecycler(it.value)
                }
                is Resource.Failure -> {
                    requireActivity().handleApiError(it)
                }
            }

        }

        viewModel.getProducts()

    }

    private fun populateRecycler(value: List<ProductResponseItem>) {
        binding.rvProduct.visibility = View.VISIBLE
        binding.rvProduct.apply {
            this.itemAnimator = DefaultItemAnimator()
            this.layoutManager = GridLayoutManager(requireContext(), 2)
            this.setHasFixedSize(true)
            this.adapter = ProductAdapter(requireActivity(), value)
        }
    }

    override fun onResume() {
        super.onResume()
        binding.shimmer.startShimmer()
    }

    override fun onPause() {
        binding.shimmer.stopShimmer()
        super.onPause()
    }

}
package com.airlift.app.ui.fragment.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.airlift.app.data.SessionManager
import com.airlift.app.data.network.Resource
import com.airlift.app.data.repository.CartRepository
import com.airlift.app.data.responses.CartItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val repository: CartRepository
) : ViewModel() {

    private val _cartResponse: MutableLiveData<MutableList<CartItem>> =
        MutableLiveData()
    val cartResponse: LiveData<MutableList<CartItem>>
        get() = _cartResponse

    fun getCart() = viewModelScope.launch {
        _cartResponse.value = SessionManager.getCart()
    }

}
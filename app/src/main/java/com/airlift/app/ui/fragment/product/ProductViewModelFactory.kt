package com.airlift.app.ui.fragment.product

import com.airlift.app.data.repository.ProductRepository
import com.airlift.app.ui.Factory

class ProductViewModelFactory(
    private val productRepository: ProductRepository
) : Factory<ProductViewModel> {

    override fun create(): ProductViewModel {
        return ProductViewModel(productRepository)
    }

}
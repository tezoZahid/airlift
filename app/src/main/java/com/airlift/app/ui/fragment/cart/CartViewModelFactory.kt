package com.airlift.app.ui.fragment.cart

import com.airlift.app.data.repository.CartRepository
import com.airlift.app.ui.Factory

class CartViewModelFactory(
    private val cartRepository: CartRepository
) : Factory<CartViewModel> {

    override fun create(): CartViewModel {
        return CartViewModel(cartRepository)
    }

}
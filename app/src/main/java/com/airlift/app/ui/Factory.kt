package com.airlift.app.ui

interface Factory<T> {
    fun create() : T
}
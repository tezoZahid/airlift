package com.airlift.app.ui.fragment.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.airlift.app.data.network.Resource
import com.airlift.app.data.repository.ProductRepository
import com.airlift.app.data.responses.ProductResponseItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(
    private val repository: ProductRepository
) : ViewModel() {

    private val _productResponse: MutableLiveData<Resource<List<ProductResponseItem>>> =
        MutableLiveData()
    val productResponse: LiveData<Resource<List<ProductResponseItem>>>
        get() = _productResponse

    fun getProducts() = viewModelScope.launch {
        _productResponse.value = repository.getProducts()
    }

}
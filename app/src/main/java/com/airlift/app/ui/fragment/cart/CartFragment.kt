package com.airlift.app.ui.fragment.cart

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.airlift.app.R
import com.airlift.app.data.responses.CartItem
import com.airlift.app.databinding.CartFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CartFragment : Fragment(R.layout.cart_fragment) {

    private val viewModel: CartViewModel by viewModels()
    private lateinit var binding: CartFragmentBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = CartFragmentBinding.bind(view)

        binding.clCartItem.visibility = View.GONE
        viewModel.cartResponse.observe(viewLifecycleOwner) {
            if (it.isNotEmpty()){
                populateRecycler(it)
            }
        }

        viewModel.getCart()

    }

    private fun populateRecycler(value: MutableList<CartItem>) {
        binding.ivEmptyCart.visibility = View.GONE
        binding.clCartItem.visibility = View.VISIBLE

        binding.rvCart.apply {
            this.itemAnimator = DefaultItemAnimator()
            this.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            this.setHasFixedSize(true)
            this.adapter = CartAdapter(requireActivity(), value)
        }

    }

}
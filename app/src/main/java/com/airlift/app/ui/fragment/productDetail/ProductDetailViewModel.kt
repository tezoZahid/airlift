package com.airlift.app.ui.fragment.productDetail

import androidx.lifecycle.ViewModel
import com.airlift.app.data.repository.ProductRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(
    private val repository: ProductRepository
) : ViewModel() {

}
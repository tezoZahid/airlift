package com.airlift.app.ui.fragment.productDetail

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.airlift.app.R
import com.airlift.app.data.SessionManager
import com.airlift.app.data.responses.CartItem
import com.airlift.app.data.responses.ProductResponseItem
import com.airlift.app.databinding.ProductDetailFragmentBinding
import com.airlift.app.util.Constant
import com.airlift.app.util.GeneralListener
import com.airlift.app.util.displayPopUp
import com.bumptech.glide.Glide

class ProductDetailFragment : Fragment(R.layout.product_detail_fragment) {

    private val viewModel: ProductDetailViewModel by viewModels()
    private lateinit var binding: ProductDetailFragmentBinding
    var productDetail = ProductResponseItem()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = ProductDetailFragmentBinding.bind(view)

        arguments?.let {
            productDetail = it.getSerializable("product") as ProductResponseItem
        }

        Log.e("ProductDetailFragment", "onViewCreated: $productDetail")

        productDetail.image.let {
            Glide.with(requireActivity())
                .load(it)
                .placeholder(R.drawable.ic_image_error_placeholder)
                .fitCenter()
                .into(binding.ivProduct)
        }

        productDetail.title?.let { binding.tvProductName.text = it }
        productDetail.price?.let { binding.tvProductPrice.text = "Rs $it" }
        productDetail.description?.let { binding.tvProductDescription.text = it }

        binding.btnAddToCart.setOnClickListener {
            SessionManager.insertCartItem(CartItem(1, productDetail.id))
            activity?.displayPopUp(
                Constant.k_Success,
                "Item Added In Cart",
                object : GeneralListener {
                    override fun buttonClick(clicked: Boolean) {
                        activity?.findNavController(R.id.navHostFragmentHome)?.popBackStack()
                    }
                }
            )
        }

    }

}
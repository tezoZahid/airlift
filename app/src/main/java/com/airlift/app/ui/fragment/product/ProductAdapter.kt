package com.airlift.app.ui.fragment.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.airlift.app.R
import com.airlift.app.data.responses.ProductResponseItem
import com.airlift.app.databinding.ItemProductBinding
import com.bumptech.glide.Glide

class ProductAdapter(
    private val activity: FragmentActivity,
    private val mList: List<ProductResponseItem>
) : RecyclerView.Adapter<ProductAdapter.ProductVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ProductVH(
        ItemProductBinding
            .inflate(LayoutInflater.from(activity), parent, false)
    )

    override fun onBindViewHolder(holder: ProductVH, position: Int) = holder.run {
        holder.bind(mList[position])
    }

    override fun getItemCount() = mList.size

    inner class ProductVH(private val binding: ItemProductBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ProductResponseItem) {

            Glide.with(activity)
                .load(item.image)
                .placeholder(R.drawable.ic_image_error_placeholder)
                .fitCenter()
                .into(binding.ivProduct)
            item.title?.let { binding.tvProductName.text = it }
            item.price?.let { binding.tvProductPrice.text = "Rs $it" }

            itemView.setOnClickListener {
                val bundle = Bundle()
                bundle.putSerializable("product", item)
                activity.findNavController(R.id.navHostFragmentHome).navigate(R.id.navProductDetail, bundle)
            }

        }
    }

}
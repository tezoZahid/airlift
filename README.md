## Directory Structure

Application contains 4 different packages. 
1- **Data**
    This package contains 3 child packages
    1- **Network** 
        This package contains all files related to network call i.e. Retrofit. Mostly, error handling is done in this package
        related to network.
    2- **Repository**
        This package contains all the repositories that are used in project.
    3- **Responses**
    This package contains all the model classes that are used in project.

One more object placed in this package is SessionManager in which all the shared preference calls are handled.

2- **DI**
    This package contains dependency injection module used to provide context to Api Interface.

3- **UI**
    All the activities are placed in that package. All the fragments are placed in nested package i.e. fragment
    1- **Fragment**
    Fragment package contains all the fragments packed in their specific packages. Each Fragment contains 
    their fragment class, View Model class and View Model factory. One additional class is adapter which
    is responsible for recycler adapter placed in their specific fragment class.

4- **UTIL**
    All the global classes used in multiple places are placed in this package.

## Note

I have followed the MVVM structure. For every network call placed in fragment or activity you have to follow
the following cycle.
    Activity/Fragment -> ViewModel -> Repository -> Api

Retrofit Object is created in RemoteDataSource and the internet connectivity check is placed there.